def add(a,b):
	print(f"{a} + {b} = {a+b}")
def sub(a,b):
	print(f"{a} - {b} = {a-b}")
def mul(a,b):
	print(f"{a} * {b} = {a*b}")
def div(a,b):
	if b!=0:
		print(f"{a} / {b} = {a/b}")
	else:
		print("Division by zero is not allowed")

if __name__=="__main__":
	a=3
	b=8
	add(a,b)
	sub(a,b)
	mul(a,b)
	div(a,b)
