import telepot
from telepot.loop import MessageLoop
from telepot.namedtuple import InlineKeyboardMarkup, InlineKeyboardButton
import json
import requests
import time
import cherrypy

class RESTBot:
    exposed=True
    def __init__(self, token):
        self.tokenBot = token
        self.bot = telepot.Bot(self.tokenBot)
        self.chatIDs=[]
        self.__message={"alert":"","action":""}
        MessageLoop(self.bot, {'chat': self.on_chat_message}).run_as_thread()

    def on_chat_message(self,msg):
        content_type, chat_type, chat_ID = telepot.glance(msg)
        self.chatIDs.append(chat_ID)

    def POST(self,*uri):
        tosend=''
        output={'status':'not-sent','message':''}
        if len(uri)!=0:
            if uri[0]=='temp':
                body=cherrypy.request.body.read()
                jsonBody=json.loads(body)
                alert=jsonBody['alert']
                action=jsonBody['action']
                tosend=f"ATTENTION!!!\n{alert}, you should {action}"
                output={'status':'sent','message':tosend}
                for user in self.chatIDs:
                    self.bot.sendMessage(user,text=tosend)
        return json.dumps(output)

if __name__ == "__main__":
    conf = json.load(open("settings.json"))
    token = conf["telegramToken"]
    rb=RESTBot(token)
    cherryConf={
		'/':{
				'request.dispatch':cherrypy.dispatch.MethodDispatcher(),
				'tool.session.on':True
		}
    }
    cherrypy.tree.mount(rb,'/',cherryConf)
    cherrypy.engine.start()
    cherrypy.engine.block()