class NBAStats():
	def __init__(self,filename):
		self.filename = filename
		self.file_content=json.load(open(filename))
	def get_players(self):
		return self.file_content['players']

	def average_heigth(self):
		players=get_players(self.file_content)
		heigths=[p['hgt']/39.37 for p in players]
		return sum(heigths)/len(heigths)

	def average_weigth(self):
		players=get_players(self.file_content)
		weigths=[p['weight']/2.205 for p in players]
		return sum(weigths)/len(weigths)

	def average_ratings(self):
		players=get_players()
		num_players=len(players)
		avg_ratings=dict.fromkeys(players[0]['ratings'][0],0)
		for player in players:
			player_ratings=player['ratings'][0]
			for key in player_ratings.keys():
				avg_ratings[key]+=player_ratings[key]/num_players
		return avg_ratings
				
	def average_age(self):
		players=get_players()
		ages=[2020-p['born']['year'] for p in players]
		return sum(ages)/len(ages)

if __name__=="__main__":
	nba=NBAStats('usefulFiles/playerNba.json')
	print(nba.average_heigth())
	print(nba.average_weigth())
	print(nba.average_ratings())
	print(nba.average_age())