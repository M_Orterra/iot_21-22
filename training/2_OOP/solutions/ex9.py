class Car():
    maxGear=6
    minGear=1
    maxSpeed=250
    minSpeed=0
    def __init__(self, name):
        self.__name = name
        self.__gear =1
        self.__speed=0
    
    def getName(self):
        return self.__name
    
    def getSpeed(self):
        return self.__speed

    def getGear(self):
        return self.__gear

    def setSpeed(self,newSpeed):
        self.__speed=min([Car.maxSpeed,max([newSpeed,Car.minSpeed])])
        return self.getSpeed()

    def increaseGear(self):
        self.__gear=min([Car.maxGear,max([Car.minGear,self.__gear+1])])
        return self.getGear()

    def decreaseGear(self):
        self.__gear=min([Car.maxGear,max([Car.minGear,self.__gear-1])])
        return self.getGear()

if __name__=="__main__":
    c=Car("Panda")
    print(f"Car name: {c.getName()}")
    print(f"Initial speed: {c.getSpeed()}")
    print(f"Initial gear: {c.getGear()}")
    
    c.setSpeed(100)
    print(f"Speed :{c.getSpeed()}")

    c.increaseGear()
    print(f"Gear: {c.getGear()}")
    c.decreaseGear()
    print(f"Gear: {c.getGear()}")

    #trying to go over limits
    #gear
    for i in range(10):
        c.increaseGear()
    print(f"Gear: {c.getGear()}")

    #speed
    c.setSpeed(300)
    print(f"Speed :{c.getSpeed()}")
