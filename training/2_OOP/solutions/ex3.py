import random

suits=['Hearts','Diamonds','Clubs','Spades']	
values=['A','2','3','4','5','6','7','8','9','10','J','Q','K']

class Card(object):
	"""Each a card has a suit and a value defined when it's created"""
	
	def __init__(self, suit, value):
		self.suit = suit
		self.value= value
	
	def __repr__(self):
		return f"{self.value} of {self.suit}"

class Deck(object):
	"""The deck is composed by 13 cards for each of the 4 suits. 
	It also has:
		a shuffle() method that returns the shffled deck
		a draw(n) method that returns a list of n cards """

	def __init__(self ):
		self.cards=[ Card(s,v) for s in suits for v in values]
	
	def shuffle(self):
		"""Shuffles the deck"""
		random.shuffle(self.cards)
	
	def draw(self,n=1):
		"""Returns n cards, 1 if n is not specified"""
		# we check if n is lower than the number of cards in the deck
		# if it is, we return the n cards
		if n<=len(self.cards):			
			drawn=[]
			for i in range(n):
				drawn.append(self.cards.pop())
			return drawn
		# if n is greater than the number of cards in the deck, we return all the cards
		elif n>len(self.cards) and len(self.cards)>0:
			# we can to this in two ways:
			#1
			# we return the remaining cards that will be equal to the number of cards
			# remaining in the deck ( len(cards))
			drawn=[]
			for i in range(len(self.cards)):
				drawn.append(self.cards.pop())
			return drawn
			#2
			# we could obtain the same result by calling the draw() method again
			# with the number of cards remaining in the deck
			# return self.draw(len(self.cards))
		else:
			return None
