import math 

class Point:
	def __init__(self,x,y):
		self.x = x
		self.y = y
	def distance(self,other):
		return math.sqrt((self.x-other.x)**2+(self.y-other.y)**2)
	def move(self,dx,dy):
		self.x += dx
		self.y += dy
		return f"{self.x}, {self.y}"
	def __repr__(self):
		return f"P: {self.x}, {self.y}"

class Line(object):
	def __init__(self,m=0,q=0):
		"""Line stored as y=mx+q"""
		self.m=m
		self.q=q
		
	def __repr__(self):		
		return f"Line: y={self.m}x+{self.q}"
	
	def line_from_points(self,pointA,pointB):
		""" line_from_points(A,B)
		Calculate the equation of the line y=mx+q crossing two points A and B"""
		m=(pointB.y-pointA.y)/(pointB.x-pointA.x)
		q=-((pointB.y-pointA.y)/(pointB.x-pointA.x))*pointA.x+pointA.y
		return Line(m,q)
		
		
	def distance(self,point):
		"""distance(point)
		Calculates the distance between the line and the given point"""
		return (abs(point.y-(self.m*point.x+self.q))/math.sqrt(1+self.m**2))
	
	def intersection(self,other):
		"""intersection(otherLine)
		Calculates the point of intersection between the two lines"""
		if self.m==other.m:
			print('The lines are parallel')
			return None
		else:
			x=(other.q-self.q)/(self.m-other.m)
			y=self.m*((other.q-self.q)/(self.m-other.m))+self.q
			return Point(x,y)
	# Optional
	# this method is executed when we ask to print an oject of this class
	# we need to return the string representation of the object
	# as l : y=mx+q
	def __repr__(self):
		return f"l: y = {self.m}x + {self.q}"
		

if __name__=="__main__":
	# 1 Simple creation
	l1=Line(m=3,q=2)
	print(l1)
	#2 Create line from 2 points
	a=Point(0,1)
	b=Point(2,2)
	# Empty line
	l2=Line()
	l2.line_from_points(a,b)
	print(l2)
	#3 Function for distance from point and intersection with another line
	l=Line(m=1,q=0)
	a=Point(1,5)
	print(l.distance(a))
	m=Line(-1,0)
	i=l.intersection(m)
	print(i)