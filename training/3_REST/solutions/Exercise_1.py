import cherrypy
class UriReverser():
    """docstring for Reverser"""

    exposed = True

    def __init__(self):
        pass

    def GET(self, *uri):
        print(len(uri))
        if len(uri)>0:
            for x in uri:
                x=x[::-1]
            return ','.join(uri)
        else:
            # you can define a simple http error message
            raise cherrypy.HTTPError(400, 'No URI given, you need to provide at least one uri')
            #or tou can provide a custom page with also the function defined below
            #raise cherrypy.HTTPError(status=400)
#def error_page_400(status, message, traceback, version):
#       return "'No URI given, you need to provide at least one uri'"

if __name__ == '__main__':
    conf = {
        '/': {
                'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
                'tool.session.on': True
        }
    }
    cherrypy.tree.mount(UriReverser(), '/', conf)
    #this is needed if you want to have the custom error page
    #cherrypy.config.update({'error_page.400': error_page_400})
    cherrypy.engine.start()
    cherrypy.engine.block()
