# Guide for using the Raspberry Pi

## Initial requirements

Before Doing anything else we need to flash the sd card to install on it the operative system.
To do that you need the following requirements:
 - Raspberry Pi imager that you can download [here](https://www.raspberrypi.com/software/)
 - The OS image that you can download [here](https://www.dropbox.com/sh/il51jojdeujnr0g/AAD924MnFlThrwiNrl8SnFIIa?dl=0)

## Flashing
Once you've downloaded the OS image and dowloaded and installed the imager software you can proceed with the flashing:

  1 Insert the SD card in your laptop
  2 Open Raspberry Pi imager

You should see something like the image below

![imager](images/imager.png)

Select "Choose OS" and at the bottom of the menu select "Use custom" and specify the image file you're going to use (i.e. **iot.img**)

![custom os](images/custom.png)
![iot.img](images/iotimg.png)

Select "Choose storage" and select the SD card, you should see something like the image below but the storage size could be different.
![storage](images/storage.png)

Then you can select write and wait until the operation is completed.
Once everithing is finished you can insert the SD card in the raspberry and you're ready to go!
